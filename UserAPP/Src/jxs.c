#include "jxs.h"
#include "tim.h"

void jxs_init()
{
	HAL_TIM_PWM_Start(&htim10, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim11, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim12, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim12, TIM_CHANNEL_2);
	HAL_TIM_PWM_Start(&htim13, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim14, TIM_CHANNEL_1);
}


#if 1
void jxs_s0()//初始位置
{
	__HAL_TIM_SetCompare(&htim10, TIM_CHANNEL_1, 2100);//云台
	__HAL_TIM_SetCompare(&htim11, TIM_CHANNEL_1, 2200);//肘关节
	__HAL_TIM_SetCompare(&htim12, TIM_CHANNEL_1, 2400);//肩关节1
	__HAL_TIM_SetCompare(&htim12, TIM_CHANNEL_2, 600);//肩关节0
	__HAL_TIM_SetCompare(&htim13, TIM_CHANNEL_1, 1400);//腕关节
	__HAL_TIM_SetCompare(&htim14, TIM_CHANNEL_1, 1100);//爪子
}

void jxs_s1()//原料准备抓取
{
	__HAL_TIM_SetCompare(&htim10, TIM_CHANNEL_1, 500);//云台
	__HAL_TIM_SetCompare(&htim11, TIM_CHANNEL_1, 1300);//肘关节
	__HAL_TIM_SetCompare(&htim12, TIM_CHANNEL_1, 500);//肩关节1
	__HAL_TIM_SetCompare(&htim12, TIM_CHANNEL_2, 2500);//肩关节0
	__HAL_TIM_SetCompare(&htim13, TIM_CHANNEL_1, 2380);//腕关节
	__HAL_TIM_SetCompare(&htim14, TIM_CHANNEL_1, 1100);//爪子
}

void jxs_s2()//原料区抓取
{
	__HAL_TIM_SetCompare(&htim10, TIM_CHANNEL_1, 500);//云台
	__HAL_TIM_SetCompare(&htim11, TIM_CHANNEL_1, 1400);//肘关节
	__HAL_TIM_SetCompare(&htim12, TIM_CHANNEL_1, 500);//肩关节1
	__HAL_TIM_SetCompare(&htim12, TIM_CHANNEL_2, 2500);//肩关节0
	__HAL_TIM_SetCompare(&htim13, TIM_CHANNEL_1, 2430);//腕关节
	__HAL_TIM_SetCompare(&htim14, TIM_CHANNEL_1, 1100);//爪子
}

void jxs_s3()//抬升原料区机械手
{
	__HAL_TIM_SetCompare(&htim10, TIM_CHANNEL_1, 500);//云台
	__HAL_TIM_SetCompare(&htim11, TIM_CHANNEL_1, 1000);//肘关节
	__HAL_TIM_SetCompare(&htim12, TIM_CHANNEL_1, 500);//肩关节1
	__HAL_TIM_SetCompare(&htim12, TIM_CHANNEL_2, 2500);//肩关节0
	__HAL_TIM_SetCompare(&htim13, TIM_CHANNEL_1, 2430);//腕关节
	__HAL_TIM_SetCompare(&htim14, TIM_CHANNEL_1, 1320);//爪子
}

void jxs_s4()//云台从原料区转到粗加工区 
{
	
	__HAL_TIM_SetCompare(&htim10, TIM_CHANNEL_1, 1500);//云台
	
	__HAL_TIM_SetCompare(&htim11, TIM_CHANNEL_1, 1000);//肘关节
	
	__HAL_TIM_SetCompare(&htim12, TIM_CHANNEL_1, 500);//肩关节1
	
	__HAL_TIM_SetCompare(&htim12, TIM_CHANNEL_2, 2500);//肩关节0
	
	__HAL_TIM_SetCompare(&htim13, TIM_CHANNEL_1, 2430);//腕关节
	
	__HAL_TIM_SetCompare(&htim14, TIM_CHANNEL_1, 1320);//爪子
}

void jxs_s5()//粗加工区准备释放
{
	
	__HAL_TIM_SetCompare(&htim10, TIM_CHANNEL_1, 1500);//云台
	
	__HAL_TIM_SetCompare(&htim11, TIM_CHANNEL_1, 1300);//肘关节
	
	__HAL_TIM_SetCompare(&htim12, TIM_CHANNEL_1, 500);//肩关节1
	
	__HAL_TIM_SetCompare(&htim12, TIM_CHANNEL_2, 2500);//肩关节0
	
	__HAL_TIM_SetCompare(&htim13, TIM_CHANNEL_1, 2400);//腕关节
	
	__HAL_TIM_SetCompare(&htim14, TIM_CHANNEL_1, 1320);//爪子
}

void jxs_s6()//粗加工释放物块
{
	
	__HAL_TIM_SetCompare(&htim10, TIM_CHANNEL_1, 1500);//云台
	
	__HAL_TIM_SetCompare(&htim11, TIM_CHANNEL_1, 1300);//肘关节
	
	__HAL_TIM_SetCompare(&htim12, TIM_CHANNEL_1, 500);//肩关节1
	
	__HAL_TIM_SetCompare(&htim12, TIM_CHANNEL_2, 2500);//肩关节0
	
	__HAL_TIM_SetCompare(&htim13, TIM_CHANNEL_1, 2400);//腕关节
	
	__HAL_TIM_SetCompare(&htim14, TIM_CHANNEL_1, 1100);//爪子松开
}

void jxs_s7()//粗加工拿取
{
	
	__HAL_TIM_SetCompare(&htim10, TIM_CHANNEL_1, 1500);//云台
	
	__HAL_TIM_SetCompare(&htim11, TIM_CHANNEL_1, 1300);//肘关节
	
	__HAL_TIM_SetCompare(&htim12, TIM_CHANNEL_1, 500);//肩关节1
	
	__HAL_TIM_SetCompare(&htim12, TIM_CHANNEL_2, 2500);//肩关节0
	
	__HAL_TIM_SetCompare(&htim13, TIM_CHANNEL_1, 2400);//腕关节
	
	__HAL_TIM_SetCompare(&htim14, TIM_CHANNEL_1, 1320);//爪子抓取
}

void jxs_s8()//抬升粗加工区机械手
{
	
	__HAL_TIM_SetCompare(&htim10, TIM_CHANNEL_1, 1500);//云台
	
	__HAL_TIM_SetCompare(&htim11, TIM_CHANNEL_1, 1000);//肘关节
	
	__HAL_TIM_SetCompare(&htim12, TIM_CHANNEL_1, 500);//肩关节1
	
	__HAL_TIM_SetCompare(&htim12, TIM_CHANNEL_2, 2500);//肩关节0
	
	__HAL_TIM_SetCompare(&htim13, TIM_CHANNEL_1, 2400);//腕关节
	
	__HAL_TIM_SetCompare(&htim14, TIM_CHANNEL_1, 1320);//爪子
}

void jxs_s9()//云台从粗加工区转到原料区
{
	
	__HAL_TIM_SetCompare(&htim10, TIM_CHANNEL_1, 500);//云台
	
	__HAL_TIM_SetCompare(&htim11, TIM_CHANNEL_1, 1000);//肘关节
	
	__HAL_TIM_SetCompare(&htim12, TIM_CHANNEL_1, 500);//肩关节1
	
	__HAL_TIM_SetCompare(&htim12, TIM_CHANNEL_2, 2500);//肩关节0
	
	__HAL_TIM_SetCompare(&htim13, TIM_CHANNEL_1, 2400);//腕关节
	
	__HAL_TIM_SetCompare(&htim14, TIM_CHANNEL_1, 1100);//爪子
}

void jxs_s10()//云台从粗加工区转到半成品区
{
	
	__HAL_TIM_SetCompare(&htim10, TIM_CHANNEL_1, 2500);//云台
	
	__HAL_TIM_SetCompare(&htim11, TIM_CHANNEL_1, 1000);//肘关节
	
	__HAL_TIM_SetCompare(&htim12, TIM_CHANNEL_1, 500);//肩关节1
	
	__HAL_TIM_SetCompare(&htim12, TIM_CHANNEL_2, 2500);//肩关节0
	
	__HAL_TIM_SetCompare(&htim13, TIM_CHANNEL_1, 2400);//腕关节
	
	__HAL_TIM_SetCompare(&htim14, TIM_CHANNEL_1, 1320);//爪子
}

void jxs_s11()//半成品区准备释放物块（低）
{
	
	__HAL_TIM_SetCompare(&htim10, TIM_CHANNEL_1, 2500);//云台
	
	__HAL_TIM_SetCompare(&htim11, TIM_CHANNEL_1, 1300);//肘关节
	
	__HAL_TIM_SetCompare(&htim12, TIM_CHANNEL_1, 500);//肩关节1
	
	__HAL_TIM_SetCompare(&htim12, TIM_CHANNEL_2, 2500);//肩关节0
	
	__HAL_TIM_SetCompare(&htim13, TIM_CHANNEL_1, 2400);//腕关节
	
	__HAL_TIM_SetCompare(&htim14, TIM_CHANNEL_1, 1320);//爪子
}

void jxs_s12()//半成品区释放物块（低）
{
	
	__HAL_TIM_SetCompare(&htim10, TIM_CHANNEL_1, 2500);//云台
	
	__HAL_TIM_SetCompare(&htim11, TIM_CHANNEL_1, 1300);//肘关节
	
	__HAL_TIM_SetCompare(&htim12, TIM_CHANNEL_1, 500);//肩关节1
	
	__HAL_TIM_SetCompare(&htim12, TIM_CHANNEL_2, 2500);//肩关节0
	
	__HAL_TIM_SetCompare(&htim13, TIM_CHANNEL_1, 2400);//腕关节
	
	__HAL_TIM_SetCompare(&htim14, TIM_CHANNEL_1, 1100);//爪子
}

//void jxs_s13()//半成品区准备释放物块（高）
//void jxs_s14()//半成品区释放物块（高）

void gx_1()// 指令1
{
	
	__HAL_TIM_SetCompare(&htim10, TIM_CHANNEL_1, 2100);//云台
	
	__HAL_TIM_SetCompare(&htim11, TIM_CHANNEL_1, 2200);//肘关节
	
	__HAL_TIM_SetCompare(&htim12, TIM_CHANNEL_1, 2400);//肩关节1
	
	__HAL_TIM_SetCompare(&htim12, TIM_CHANNEL_2, 600);//肩关节0
	
	__HAL_TIM_SetCompare(&htim13, TIM_CHANNEL_1, 1400);//腕关节
	
	__HAL_TIM_SetCompare(&htim14, TIM_CHANNEL_1,1100);//爪子
}

void gx_2()//指令2
{	
	__HAL_TIM_SetCompare(&htim13, TIM_CHANNEL_1, 2380);//腕关节
	__HAL_TIM_SetCompare(&htim10, TIM_CHANNEL_1, 500);//云台
	__HAL_TIM_SetCompare(&htim11, TIM_CHANNEL_1, 1300);//肘关节
	__HAL_TIM_SetCompare(&htim12, TIM_CHANNEL_1, 500);//肩关节1
	__HAL_TIM_SetCompare(&htim12, TIM_CHANNEL_2, 2500);//肩关节0
	__HAL_TIM_SetCompare(&htim14, TIM_CHANNEL_1, 1100);//爪子
}

//void gx_3()//指令3 爪子闭合，两辆车不同，重要

void gx_4()// 指令4
{
	jxs_s3();
	HAL_Delay(3000);
	jxs_s4();
	HAL_Delay(3000);
	jxs_s5();
}

void gx_5()// 指令5
{
	jxs_s8();
	HAL_Delay(3000);
	jxs_s10();
	HAL_Delay(3000);
	jxs_s11();
}


#endif
