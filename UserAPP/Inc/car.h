#ifndef __CAR_H
#define __CAR_H
#ifdef __cplusplus
 extern "C" {
#endif

#include "main.h"

typedef struct _Point{
    uint8_t x;
    uint8_t y;
} Point;

typedef enum _Dir{
    dir_stop=0,
    dir_f,
    dir_b,
    dir_l,
    dir_r
} Dir;

typedef struct _CarInfo
{
    Point currentPoint;
    Point targetPoint;
    Dir dir;
    uint8_t speed;
    uint8_t Flag_GetLine;
} CarInfo;


extern CarInfo myCarInfo;

void Car_Init();

#ifdef __cplusplus
}
#endif
#endif /* __CAR_H */
