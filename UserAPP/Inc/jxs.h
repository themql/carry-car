#ifndef __JXS_H
#define __JXS_H
#ifdef __cplusplus
 extern "C" {
#endif

#include "main.h"

void jxs_init();
void jxs_s0();  //初始位置
void jxs_s1();  //原料准备抓取
void jxs_s2();  //原料区抓取
void jxs_s3();  //抬升原料区机械手
void jxs_s4();  //云台从原料区转到粗加工区 
void jxs_s5();  //粗加工区准备释放
void jxs_s6();  //粗加工释放物块
void jxs_s7();  //粗加工拿取
void jxs_s8();  //抬升粗加工区机械手
void jxs_s9();  //云台从粗加工区转到原料区
void jxs_s10(); //云台从粗加工区转到半成品区
void jxs_s11(); //半成品区准备释放物块（低）
void jxs_s12(); //半成品区释放物块（低）

void gx_1();    //指令1
void gx_2();    //指令2
void gx_4();    //指令4
void gx_5();    //指令5

#ifdef __cplusplus
}
#endif
#endif /* __JXS_H */