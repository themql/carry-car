#ifndef __TRACKSENSOR_H
#define __TRACKSENSOR_H
#ifdef __cplusplus
 extern "C" {
#endif


#include "main.h"
#include "car.h"

#define FRONT_R2    HAL_GPIO_ReadPin(TS_front_R2_GPIO_Port,TS_front_R2_Pin)  
#define FRONT_R1    HAL_GPIO_ReadPin(TS_front_R1_GPIO_Port,TS_front_R1_Pin)
#define FRONT_L1    HAL_GPIO_ReadPin(TS_front_L1_GPIO_Port,TS_front_L1_Pin)
#define FRONT_L2    HAL_GPIO_ReadPin(TS_front_L2_GPIO_Port,TS_front_L2_Pin)
#define SIDE_R2     HAL_GPIO_ReadPin(TS_side_R2_GPIO_Port,TS_side_R2_Pin)
#define SIDE_R1     HAL_GPIO_ReadPin(TS_side_R1_GPIO_Port,TS_side_R1_Pin)
#define SIDE_L1     HAL_GPIO_ReadPin(TS_side_L1_GPIO_Port,TS_side_L1_Pin)
#define SIDE_L2     HAL_GPIO_ReadPin(TS_side_L2_GPIO_Port,TS_side_L2_Pin)



void TS_Scan(int* PWM_Adj_Arr, CarInfo* P_CarInfo);


#ifdef __cplusplus
}
#endif
#endif /* __TRACKSENSOR_H */