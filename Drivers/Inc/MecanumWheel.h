#ifndef __MECANUMWHEEL_H
#define __MECANUMWHEEL_H
#ifdef __cplusplus
 extern "C" {
#endif

#include "main.h"


void MW_Stop();
void MW_Go_Forward(uint16_t speed);
void MW_Go_Backward(uint16_t speed);
void MW_Pan_Left(uint16_t speed);
void MW_Pan_Right(uint16_t speed);
void MW_Rotate_Clkwise(uint16_t speed);
void MW_Rotate_Anticlkwise(uint16_t speed);
void MW_Diagonal_FL(uint16_t speed);
void MW_Diagonal_FR(uint16_t speed);
void MW_Diagonal_BL(uint16_t speed);
void MW_Diagonal_BR(uint16_t speed);

#ifdef __cplusplus
}
#endif
#endif /* __MECANUMWHEEL_H */