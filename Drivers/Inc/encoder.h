#ifndef __ENCODER_H
#define __ENCODER_H
#ifdef __cplusplus
 extern "C" {
#endif


#include "main.h"

extern volatile int32_t Encoder_CNT_Increment[4];
extern volatile int16_t Encoder_Overflow_CNT[4];
#define Encoder_Overflow_CNT_FL Encoder_Overflow_CNT[0]
#define Encoder_Overflow_CNT_FR Encoder_Overflow_CNT[1]
#define Encoder_Overflow_CNT_BL Encoder_Overflow_CNT[2]
#define Encoder_Overflow_CNT_BR Encoder_Overflow_CNT[3]

#define HTIM_ENCODER_FL     (htim2)
#define HTIM_ENCODER_FR     (htim3)
#define HTIM_ENCODER_BL     (htim4)
#define HTIM_ENCODER_BR     (htim5)

#define ENCODER_RESOLUTION  (11*4)


void Encoder_Init();
void Encoder_Caculate();


#ifdef __cplusplus
}
#endif
#endif /* __ENCODER_H */