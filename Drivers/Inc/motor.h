#ifndef __MOTOR_H
#define __MOTOR_H
#ifdef __cplusplus
 extern "C" {
#endif

#include "main.h"
#include "tim.h"


#define MOTOR_REDUCTION_RATIO   (50) 

#define HTIM_MOTOR_FL (htim1)
#define HTIM_MOTOR_FR (htim1)
#define HTIM_MOTOR_BL (htim8)
#define HTIM_MOTOR_BR (htim8)
#define CHANNEL_FL_A (TIM_CHANNEL_1)
#define CHANNEL_FL_B (TIM_CHANNEL_2)
#define CHANNEL_FR_A (TIM_CHANNEL_3)
#define CHANNEL_FR_B (TIM_CHANNEL_4)
#define CHANNEL_BL_A (TIM_CHANNEL_1)
#define CHANNEL_BL_B (TIM_CHANNEL_2)
#define CHANNEL_BR_A (TIM_CHANNEL_3)
#define CHANNEL_BR_B (TIM_CHANNEL_4)

#if 1   // 电机基本控制
#define FL_FORWARD(PWM_VALUE) do{                                       \
    __HAL_TIM_SET_COMPARE(&HTIM_MOTOR_FL, CHANNEL_FL_A, (PWM_VALUE) );        \
    __HAL_TIM_SET_COMPARE(&HTIM_MOTOR_FL, CHANNEL_FL_B, 0 );                  \
}while(0)

#define FL_BACKWARD(PWM_VALUE) do{                                      \
    __HAL_TIM_SET_COMPARE(&HTIM_MOTOR_FL, CHANNEL_FL_A, 0 );                  \
    __HAL_TIM_SET_COMPARE(&HTIM_MOTOR_FL, CHANNEL_FL_B, (PWM_VALUE) );        \
}while(0)

#define FR_FORWARD(PWM_VALUE) do{                                       \
    __HAL_TIM_SET_COMPARE(&HTIM_MOTOR_FR, CHANNEL_FR_A, (PWM_VALUE) );        \
    __HAL_TIM_SET_COMPARE(&HTIM_MOTOR_FR, CHANNEL_FR_B, 0 );                  \
}while(0)

#define FR_BACKWARD(PWM_VALUE) do{                                      \
    __HAL_TIM_SET_COMPARE(&HTIM_MOTOR_FR, CHANNEL_FR_A, 0 );                  \
    __HAL_TIM_SET_COMPARE(&HTIM_MOTOR_FR, CHANNEL_FR_B, (PWM_VALUE) );        \
}while(0)

#define BL_FORWARD(PWM_VALUE) do{                                       \
    __HAL_TIM_SET_COMPARE(&HTIM_MOTOR_BL, CHANNEL_BL_A, (PWM_VALUE) );        \
    __HAL_TIM_SET_COMPARE(&HTIM_MOTOR_BL, CHANNEL_BL_B, 0 );                  \
}while(0)

#define BL_BACKWARD(PWM_VALUE) do{                                      \
    __HAL_TIM_SET_COMPARE(&HTIM_MOTOR_BL, CHANNEL_BL_A, 0 );                  \
    __HAL_TIM_SET_COMPARE(&HTIM_MOTOR_BL, CHANNEL_BL_B, (PWM_VALUE) );        \
}while(0)

#define BR_FORWARD(PWM_VALUE) do{                                       \
    __HAL_TIM_SET_COMPARE(&HTIM_MOTOR_BR, CHANNEL_BR_A, (PWM_VALUE) );        \
    __HAL_TIM_SET_COMPARE(&HTIM_MOTOR_BR, CHANNEL_BR_B, 0 );                  \
}while(0)

#define BR_BACKWARD(PWM_VALUE) do{                                      \
    __HAL_TIM_SET_COMPARE(&HTIM_MOTOR_BR, CHANNEL_BR_A, 0 );                  \
    __HAL_TIM_SET_COMPARE(&HTIM_MOTOR_BR, CHANNEL_BR_B, (PWM_VALUE) );        \
}while(0)

#endif

// 速度代号
extern int PWM[4];
#define PWM_FL PWM[0]
#define PWM_FR PWM[1]
#define PWM_BL PWM[2]
#define PWM_BR PWM[3]

extern int Target_Speed[4];
#define Target_FL Target_Speed[0]
#define Target_FR Target_Speed[1]
#define Target_BL Target_Speed[2]
#define Target_BR Target_Speed[3]

#define Speed_Level0 (0)
#define Speed_Level1 (25)
#define Speed_Level2 (45)

// 四轮 PWM控制
extern uint8_t MOTOR_Dir[4];
#define MOTOR_Dir_FL MOTOR_Dir[0]
#define MOTOR_Dir_FR MOTOR_Dir[1]
#define MOTOR_Dir_BL MOTOR_Dir[2]
#define MOTOR_Dir_BR MOTOR_Dir[3]

#define PWM_MAX (100)
#define PWM_MIN ( 25)
// #define PWM_Limit(PWM)  (PWM > PWM_MAX ?PWM_MAX :(PWM < PWM_MIN ?PWM_MIN :PWM))
#define PWM_Limit(PWM)  (PWM > PWM_MAX ?PWM_MAX :PWM )

void Motor_init();
void Motor_Target_Limit(int16_t* Target_Array);
int Motor_PI_FL(int Encoder_Value, int Target);
int Motor_PI_FR(int Encoder_Value, int Target);
int Motor_PI_BL(int Encoder_Value, int Target);
int Motor_PI_BR(int Encoder_Value, int Target);

#ifdef __cplusplus
}
#endif
#endif /* __MOTOR_H */
