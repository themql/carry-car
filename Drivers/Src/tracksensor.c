#include "tracksensor.h"


#define PWM_Adj_val (5)

void TS_Scan(int* PWM_Adj_Arr, CarInfo* P_CarInfo)
{
    // 在线上为高(灯灭)
    if(P_CarInfo->dir == dir_f){
        if((FRONT_L1 == 0)&&(FRONT_R1 == 1)){
            // 左偏
            PWM_Adj_Arr[0]= PWM_Adj_val;
            PWM_Adj_Arr[1]=-PWM_Adj_val;
        }
        else if((FRONT_L1 == 1)&&(FRONT_R1 == 0)){
            // 右偏
            PWM_Adj_Arr[0]=-PWM_Adj_val;
            PWM_Adj_Arr[1]= PWM_Adj_val;
        }
        else if((FRONT_L2 == 0)&&(FRONT_R2 == 0)){
            P_CarInfo->Flag_GetLine=0;
        }
        else if((P_CarInfo->Flag_GetLine == 0)&&(FRONT_L2 == 1)&&(FRONT_L1 == 1)&&(FRONT_R1 == 1)&&(FRONT_R2 == 1)){
            // 探测到整根垂直线
            P_CarInfo->currentPoint.y++;
            P_CarInfo->Flag_GetLine=1;
        }
    }
    else if(P_CarInfo->dir == dir_b){
        if((FRONT_L1 == 0)&&(FRONT_R1 == 1)){
            // 左偏
            // PWM_Adj_Arr[0]= PWM_Adj_val;
            // PWM_Adj_Arr[1]=-PWM_Adj_val;
        }
        else if((FRONT_L1 == 1)&&(FRONT_R1 == 0)){
            // 右偏
            // PWM_Adj_Arr[0]=-PWM_Adj_val;
            // PWM_Adj_Arr[1]= PWM_Adj_val;
        }
        else if((FRONT_L2 == 0)&&(FRONT_R2 == 0)){
            P_CarInfo->Flag_GetLine=0;
        }
        else if((P_CarInfo->Flag_GetLine == 0)&&(FRONT_L2 == 1)&&(FRONT_L1 == 1)&&(FRONT_R1 == 1)&&(FRONT_R2 == 1)){
            // 探测到整根垂直线
            P_CarInfo->currentPoint.y--;
            P_CarInfo->Flag_GetLine=1;
        }
    }
    else if(P_CarInfo->dir == dir_l){
        if((SIDE_L1 == 0)&&(SIDE_R1 == 1)){
            PWM_Adj_Arr[0]=-PWM_Adj_val;
            PWM_Adj_Arr[2]= PWM_Adj_val;
        }
        else if((SIDE_L1 == 1)&&(SIDE_R1 == 0)){
            PWM_Adj_Arr[0]= PWM_Adj_val;
            PWM_Adj_Arr[2]=-PWM_Adj_val;
        }
        else if((SIDE_L2 == 0)&&(SIDE_R2 == 0)){
            P_CarInfo->Flag_GetLine=0;
        }
        else if((P_CarInfo->Flag_GetLine == 0)&&(SIDE_L2 == 1)&&(SIDE_L1 == 1)&&(SIDE_R1 == 1)&&(SIDE_R2 == 1)){
            P_CarInfo->currentPoint.x++;
            P_CarInfo->Flag_GetLine=1;
        }
    }
    else if(P_CarInfo->dir == dir_r){
        if((SIDE_L1 == 0)&&(SIDE_R1 == 1)){

        }
        else if((SIDE_L1 == 1)&&(SIDE_R1 == 0)){
            
        }
        else if((SIDE_L2 == 0)&&(SIDE_R2 == 0)){
            P_CarInfo->Flag_GetLine=0;
        }
        else if((P_CarInfo->Flag_GetLine == 0)&&(SIDE_L2 == 1)&&(SIDE_L1 == 1)&&(SIDE_R1 == 1)&&(SIDE_R2 == 1)){
            P_CarInfo->currentPoint.x--;
            P_CarInfo->Flag_GetLine=1;
        }
    }
}
