#include "tim.h"
#include "BTProtocol.h"
#include "motor.h"
#include "MecanumWheel.h"
#include "oled.h"
#include "car.h"
#include "jxs.h"


#define BT_Motor_Speed (20)
uint8_t Flag_PawStatus=0;   // 0:开 1:闭

void CMDAnalysis(char* cmd)
{
    if(0xFF == cmd[0] &&0x00 == cmd[3] &&'\0' == cmd[4]){
        if(0xF0 == cmd[1]){
            // OLED_ShowNum(63, 0, cmd[2], 4, 16);
            switch (cmd[2])
            {
            case 0x01:
                // 停止
                MW_Stop();
                myCarInfo.dir=dir_stop;
                myCarInfo.speed=0;
                break;

            case 0x02:
                // 前进
                MW_Go_Forward(BT_Motor_Speed);
                myCarInfo.dir=dir_f;
                myCarInfo.speed=BT_Motor_Speed;
                break;

            case 0x03:
                // 后退
                MW_Go_Backward(BT_Motor_Speed);
                myCarInfo.dir=dir_b;
                myCarInfo.speed=BT_Motor_Speed;
                break;

            case 0x04:
                // 左平移
                MW_Pan_Left(BT_Motor_Speed);
                myCarInfo.dir=dir_l;
                myCarInfo.speed=BT_Motor_Speed;
                break;

            case 0x05:
                // 右平移
                MW_Pan_Right(BT_Motor_Speed);
                myCarInfo.dir=dir_r;
                myCarInfo.speed=BT_Motor_Speed;
                break;

            case 0x06:
                // 顺时针旋转
                MW_Rotate_Clkwise(BT_Motor_Speed);
                break;

            case 0x07:
                // 逆时针旋转
                MW_Rotate_Anticlkwise(BT_Motor_Speed);
                break;

            case 0x08:
                // 斜向_左上
                MW_Diagonal_FL(BT_Motor_Speed);
                break;

            case 0x09:
                // 斜向_右上
                MW_Diagonal_FR(BT_Motor_Speed);
                break;

            case 0x0A:
                // 斜向_左下
                MW_Diagonal_BL(BT_Motor_Speed);
                break;

            case 0x0B:
                // 斜向_右下
                MW_Diagonal_BR(BT_Motor_Speed);
                break;

            case 0x0C:
                // test 自定义
                MW_Go_Forward(BT_Motor_Speed);
                break;

            default:
                return;
                // break;
            }
        }
        else if(0x0F == cmd[1]){
            switch (cmd[2]){
                case 0x01:
                    // 初始位置
                    gx_1();
                    break;

                case 0x02:
                    // 抓取位置
                    gx_2();
                    break;

                case 0x03:
                    // 爪子状态切换
                    if(0 == Flag_PawStatus){
                        // 闭合
                        Flag_PawStatus=1;
                    	HAL_TIM_PWM_Start(&htim14, TIM_CHANNEL_1);
	                    __HAL_TIM_SetCompare(&htim14, TIM_CHANNEL_1, 1320);//爪子
                    }
                    else{
                        Flag_PawStatus=0;
                        HAL_TIM_PWM_Start(&htim14, TIM_CHANNEL_1);
	                    __HAL_TIM_SetCompare(&htim14, TIM_CHANNEL_1, 1100);//爪子
                    }
                    break;

                case 0x04:
                    // 原料到粗加工（肘关节上升、云台旋转，肘关节下降）
                    gx_4();
                    break;

                case 0x05:
                    // 粗加工到半成品（肘关节上升、云台旋转，肘关节下降）
                    gx_5();
                    break;
            }
        }
    }
    else{
        return;
    }
}


