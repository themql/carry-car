## 引脚配置

#### 电机

| 用途        | 引脚-接线脚 | 配置      | 备注                     |
| ----------- | ----------- | --------- | ------------------------ |
| motor 1_A   | PE9-F4      | TIM1: CH1 | FL(驱动板接线需重新确认) |
| motor 1_B   | PE11-F3     | TIM1: CH2 |                          |
| motor 2_A   | PE13-F2     | TIM1: CH3 | FR                       |
| motor 2_B   | PE14-F1     | TIM1: CH4 |                          |
| motor 3_A   | PC6-B4      | TIM8: CH1 | BL                       |
| motor 3_B   | PC7-B3      | TIM8: CH2 |                          |
| motor 4_A   | PC8-B2      | TIM8: CH3 | BR                       |
| motor 4_B   | PC9-B1      | TIM8: CH4 |                          |

#### 编码器

| 用途        | 引脚-接线脚 | 配置      | 备注                     |
| ----------- | ----------- | --------- | ------------------------ |
| encoder 1_A | PA5-FLB     | TIM2      | (接线需重新确认)         |
| encoder 1_B | PB3-FLA     |           |                          |
| encoder 2_A | PA6-FRA     | TIM3      |                          |
| encoder 2_B | PA7-FRB     |           |                          |
| encoder 3_A | PD12-BLB    | TIM4      |                          |
| encoder 3_B | PD13-BLA    |           |                          |
| encoder 4_A | PA0-BRA     | TIM5      |                          |
| encoder 4_B | PA1-BRB     |           |                          |

#### 串口

| 用途        | 引脚-接线脚 | 配置      | 备注                     |
| ----------- | ----------- | --------- | ------------------------ |
| UART1_TX    | PA9         |           | BlueTooth                |
| UART1_RX    | PA10        |           |                          |
| UART2_TX    | PD5         |           | debug                    |
| UART2_Rx    | PD6         |           |                          |

#### OLED(4Pin IIC)

| 用途        | 引脚-接线脚 | 配置      | 备注                     |
| ----------- | ----------- | --------- | ------------------------ |
| OLED_SDA 	  | PA2         |			|		                   |
| OLED_SCL    | PA3         |	        |                          |

#### 巡线传感器

| 用途        | 引脚-接线脚 | 配置 | 备注 |
| ----------- | ----------- | ---- | ---- |
| TS_front_L2 | PE0         |      | out6 |
| TS_front_L1 | PE1         |      | out5 |
| TS_front_R1 | PE2         |      | out4 |
| TS_front_R2 | PE3         |      | out3 |
| TS_side_L2  | PE4         |      | out5 |
| TS_side_L1  | PE5         |      | out4 |
| TS_side_R1  | PE6         |      | out3 |
| TS_side_R2  | PE7         |      | out2 |

#### 机械臂舵机

| 用途      | 引脚-接线脚 | 配置       | 备注 |
| --------- | ----------- | ---------- | ---- |
| arm servo | PF6         | TIm10: CH1 |      |
| arm servo | PF7         | TIM11: CH1 |      |
| arm servo | PB14        | TIM12: CH1 |      |
| arm servo | PB15        | TIM12: CH2 |      |
| arm servo | PF8         | TIM13: CH1 |      |
| arm servo | PF9         | TIM14: CH1 |      |



## 蓝牙协议

| 功能       | 字符串         | 备注 |
| ---------- | -------------- | ---- |
| 停止       | FF  F0  01  00 |      |
| 前进       | FF  F0  02  00 |      |
| 后退       | FF  F0  03  00 |      |
| 左平移     | FF  F0  04  00 |      |
| 右平移     | FF  F0  05  00 |      |
| 逆时针旋转 | FF  F0  06  00 |      |
| 顺时针旋转 | FF  F0  07  00 |      |
| 斜向_左上  | FF  F0  08  00 |      |
| 斜向_右上  | FF  F0  09  00 |      |
| 斜向_左下  | FF  F0  0A  00 |      |
| 斜向_右下  | FF  F0  0B  00 |      |
| test       | FF  F0  0C  00 |      |

