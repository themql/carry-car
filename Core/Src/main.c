/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "dma.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>
#include <string.h>
#include "motor.h"
#include "MecanumWheel.h"
#include "BTProtocol.h"
#include "oled.h"
#include "encoder.h"
#include "tracksensor.h"
#include "car.h"
#include "jxs.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
uint8_t data_buff[MAX_BUFF_LEN]  = {0};
uint8_t dma_buff[MAX_BUFF_LEN]   = {0};
uint8_t recv_len = 0;
// int32_t Step_CNT=0;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_TIM2_Init();
  MX_TIM3_Init();
  MX_TIM4_Init();
  MX_USART1_UART_Init();
  MX_TIM1_Init();
  MX_TIM5_Init();
  MX_TIM8_Init();
  MX_TIM6_Init();
  MX_TIM7_Init();
  MX_USART2_UART_Init();
  MX_TIM10_Init();
  MX_TIM11_Init();
  MX_TIM12_Init();
  MX_TIM13_Init();
  MX_TIM14_Init();
  /* USER CODE BEGIN 2 */
  Motor_init();
  Encoder_Init();
  myUART_Init();
  jxs_init();

  OLED_Init();
  OLED_ShowString(0, 0, (uint8_t*)"test", 16);
  HAL_Delay(1000);
  OLED_Fill(0, 0, 127, 63 ,0);

  // 测试电机
  // printf("test\n");
  // FL_FORWARD(100);
  // FR_FORWARD(100);
  // BL_FORWARD(100);
  // BR_FORWARD(100);
  // Target_FR = Speed_Level1;
  // Target_FL = Speed_Level1;
  // Target_BL = Speed_Level1;
  // Target_BR = Speed_Level1;
  
  Car_Init();
  // myCarInfo.dir=dir_l;

  
  // Task1
  // Step_CNT=0;
  // while(1){
  //   printf("%d\n",Step_CNT);
  // }

  
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    // 蓝牙指令接收与解析
    if(recv_len > 0){
      CMDAnalysis((char*)data_buff);
      // printf("%s\n", data_buff);
      recv_len = 0;
    }
  //  OLED_ShowNum(0,0,myCarInfo.currentPoint.x,2,16);
  //  OLED_ShowNum(0,16,myCarInfo.currentPoint.y,2,16);
	// printf("x:%d, y:%d, flag: %d\n",myCarInfo.currentPoint.x, myCarInfo.currentPoint.y, myCarInfo.Flag_GetLine);
  
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 168;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
int fputc(int ch,FILE *f)
{
    uint8_t temp[1]= {ch};
    HAL_UART_Transmit(&huart2,temp,1,2);
    return 0;
}

void HAL_Delay_US(uint16_t Delay)
{
    uint16_t tickstart = __HAL_TIM_GetCounter(&htim6);
    uint16_t wait = Delay;

    HAL_TIM_Base_Start(&htim6);
    while((__HAL_TIM_GetCounter(&htim6) - tickstart) < wait) {}
    HAL_TIM_Base_Stop(&htim6);
    __HAL_TIM_SetCounter(&htim6, 0);
}

void UART_IDLE_Callback(UART_HandleTypeDef *huart)
{
  extern DMA_HandleTypeDef hdma_usart1_rx;
  if(__HAL_UART_GET_FLAG(huart, UART_FLAG_IDLE) != RESET){
    __HAL_UART_CLEAR_IDLEFLAG(huart);
    HAL_UART_DMAStop(huart);
    if( &huart1 == huart){
      recv_len = MAX_BUFF_LEN - __HAL_DMA_GET_COUNTER(&hdma_usart1_rx);
      if(recv_len < MAX_BUFF_LEN - 1){
        memcpy(data_buff, dma_buff, recv_len);
        data_buff[recv_len] = '\0';
      }
      else{
        recv_len = 0;
      }
    }

    __HAL_UNLOCK(huart);
    HAL_UART_Receive_DMA(huart, dma_buff, MAX_BUFF_LEN);
  }
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  if(htim == (&htim7) ){
    int PWM_Adj[4] = {0},
        target[4]={0};
    
    Encoder_Caculate();
    // Step_CNT += (Encoder_CNT_Increment[0]+Encoder_CNT_Increment[1]
    //             +Encoder_CNT_Increment[2]+Encoder_CNT_Increment[3])/4;

    // 循迹调整
    // TS_Scan(PWM_Adj, &myCarInfo);
    // target[0]=Target_FL+PWM_Adj[0];
    // target[1]=Target_FR+PWM_Adj[1];
    // target[2]=Target_BL+PWM_Adj[2];
    // target[3]=Target_BR+PWM_Adj[3];

    // 四轮pi速度环
    // PWM_FL = Motor_PI_FL(Encoder_CNT_Increment[0], target[0]);
    // PWM_FR = Motor_PI_FR(Encoder_CNT_Increment[1], target[1]);
    // PWM_BL = Motor_PI_BL(Encoder_CNT_Increment[2], target[2]);
    // PWM_BR = Motor_PI_BR(Encoder_CNT_Increment[3], target[3]);
    PWM_FL = Motor_PI_FL(Encoder_CNT_Increment[0], Target_FL);
    PWM_FR = Motor_PI_FR(Encoder_CNT_Increment[1], Target_FR);
    PWM_BL = Motor_PI_BL(Encoder_CNT_Increment[2], Target_BL);
    PWM_BR = Motor_PI_BR(Encoder_CNT_Increment[3], Target_BR);
    
    if(PWM_FL > 0)  FL_FORWARD(PWM_Limit(PWM_FL));
    else            FL_BACKWARD(PWM_Limit(-PWM_FL));
    if(PWM_FR > 0)  FR_FORWARD(PWM_Limit(PWM_FR));
    else            FR_BACKWARD(PWM_Limit(-PWM_FR));
    if(PWM_BL > 0)  BL_FORWARD(PWM_Limit(PWM_BL));
    else            BL_BACKWARD(PWM_Limit(-PWM_BL));
    if(PWM_BR > 0)  BR_FORWARD(PWM_Limit(PWM_BR));
    else            BR_BACKWARD(PWM_Limit(-PWM_BR));
    // printf("%d %d %d %d\n",__HAL_TIM_GetCounter(&HTIM_ENCODER_FL),__HAL_TIM_GetCounter(&HTIM_ENCODER_FR),__HAL_TIM_GetCounter(&HTIM_ENCODER_BL),__HAL_TIM_GetCounter(&HTIM_ENCODER_BR));
    // printf("%d,%d,%d,%d\n",Encoder_CNT_Increment[0],Encoder_CNT_Increment[1],Encoder_CNT_Increment[2],Encoder_CNT_Increment[3]);
    // printf("%d %d %d %d\n",PWM_FL,PWM_FR,PWM_BL,PWM_BR);
  }
  else if(htim == (&HTIM_ENCODER_FL) ){
    if(__HAL_TIM_IS_TIM_COUNTING_DOWN(&HTIM_ENCODER_FL))
      --Encoder_Overflow_CNT_FL;
    else
      ++Encoder_Overflow_CNT_FL;
  }
  else if(htim == (&HTIM_ENCODER_FR) ){
    if(__HAL_TIM_IS_TIM_COUNTING_DOWN(&HTIM_ENCODER_FR))
      --Encoder_Overflow_CNT_FR;
    else
      ++Encoder_Overflow_CNT_FR;
  }
  else if(htim == (&HTIM_ENCODER_BL) ){
    if(__HAL_TIM_IS_TIM_COUNTING_DOWN(&HTIM_ENCODER_BL))
      --Encoder_Overflow_CNT_BL;
    else
      ++Encoder_Overflow_CNT_BL;
  }
  else if(htim == (&HTIM_ENCODER_BR) ){
    if(__HAL_TIM_IS_TIM_COUNTING_DOWN(&HTIM_ENCODER_BR))
      --Encoder_Overflow_CNT_BR;
    else
      ++Encoder_Overflow_CNT_BR;
  }
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
