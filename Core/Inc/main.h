/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
extern void UART_IDLE_Callback(UART_HandleTypeDef *huart);
void HAL_Delay_US(uint16_t Delay);
/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define TS_front_R1_Pin GPIO_PIN_2
#define TS_front_R1_GPIO_Port GPIOE
#define TS_front_R2_Pin GPIO_PIN_3
#define TS_front_R2_GPIO_Port GPIOE
#define TS_side_L2_Pin GPIO_PIN_4
#define TS_side_L2_GPIO_Port GPIOE
#define TS_side_L1_Pin GPIO_PIN_5
#define TS_side_L1_GPIO_Port GPIOE
#define TS_side_R1_Pin GPIO_PIN_6
#define TS_side_R1_GPIO_Port GPIOE
#define TS_side_R2_Pin GPIO_PIN_13
#define TS_side_R2_GPIO_Port GPIOC
#define OLED_SDA_Pin GPIO_PIN_2
#define OLED_SDA_GPIO_Port GPIOA
#define OLED_SCL_Pin GPIO_PIN_3
#define OLED_SCL_GPIO_Port GPIOA
#define TS_front_L2_Pin GPIO_PIN_0
#define TS_front_L2_GPIO_Port GPIOE
#define TS_front_L1_Pin GPIO_PIN_1
#define TS_front_L1_GPIO_Port GPIOE
/* USER CODE BEGIN Private defines */
#define MAX_BUFF_LEN (16)
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
